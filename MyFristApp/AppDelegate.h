//
//  AppDelegate.h
//  MyFristApp
//
//  Created by Richard Stewing on 10.07.13.
//  Copyright (c) 2013 Richard Stewing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
